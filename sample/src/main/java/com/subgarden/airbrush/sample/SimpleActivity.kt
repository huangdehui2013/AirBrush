package com.subgarden.airbrush.sample

import android.os.Bundle
import android.os.StrictMode
import android.os.StrictMode.VmPolicy
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import com.subgarden.airbrush.AirBrush
import com.subgarden.airbrush.loaders.GradientPalette
import com.subgarden.airbrush.loaders.TinyThumb
import kotlinx.android.synthetic.main.activity_simple.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class SimpleActivity : AppCompatActivity() {

    companion object {
        const val SPAN_COUNT = 3
    }

    private lateinit var adapter: GridAdapter
    private var dataSource: StaticDataSource? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_simple)

        // Detect any potential issues early.
        // If you see a StrictMode violation, please make an issue in GitLab.
        StrictMode.setThreadPolicy(
            StrictMode.ThreadPolicy.Builder()
                .detectAll()
                .penaltyLog()
                .penaltyDeath()
                .build()
        )
        StrictMode.setVmPolicy(
            VmPolicy.Builder()
                .detectAll()
                .penaltyLog()
                .penaltyDeath()
                .build()
        )

        gradientPaletteRadioButton.text = GradientPalette::class.java.simpleName
        gradientPaletteRadioButton.isEnabled = false

        tinyThumbRadioButton.text = TinyThumb::class.java.simpleName
        tinyThumbRadioButton.isEnabled = false

        adapter = GridAdapter(GlideApp.with(this))
        recyclerView.adapter = adapter

        // The data source takes time to initialise. Do it in the background to satisfy StrictMode.
        GlobalScope.launch {
            dataSource = StaticDataSource(this@SimpleActivity).also {
                withContext(Dispatchers.Main) {
                    adapter.submitList(it.gradientPaletteData)

                    gradientPaletteRadioButton.isEnabled = true
                    tinyThumbRadioButton.isEnabled = true

                    radioGroup.check(gradientPaletteRadioButton.id)
                }
            }
        }

        gradientPaletteRadioButton.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                dataSource?.let { adapter.submitList(it.gradientPaletteData) }
            }
        }

        tinyThumbRadioButton.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                dataSource?.let { adapter.submitList(it.tinyThumbData) }
            }
        }

        val layoutManager = GridLayoutManager(this, SPAN_COUNT)
        recyclerView.layoutManager = layoutManager

        reloadButton.setOnClickListener {
            // Invalidate all items.
            // Normally notifyDataSetChanged would be used, but Glide's crossfade sometimes continues
            // after onBind is called again, causing a glitch.
            // This is normally not a problem, but for demo purposes it looks less than ideal.
            recyclerView.adapter = adapter
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        // After spraying paint all over the place it's time to clean up.
        // Luckily this is quick and easy. Normally you'd call cleanup() when you're down using AirBrush
        // for a good while. Note that it's safe to use AirBrush again, even after cleanup() has been called.
        AirBrush.cleanup()
    }
}
